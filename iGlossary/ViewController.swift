//
//  ViewController.swift
//  iGlossary
//
//  Created by A Khairini on 13/04/20.
//  Copyright © 2020 Infinite Learning ADA. All rights reserved.
//

import UIKit

class Glossaries {
    var terms: String?
    var definitions: String?
    var links: String?
    
    init(termName:String, termDefinition:String, termLink:String){
        
        self.terms = termName
        self.definitions = termDefinition
        self.links = termLink
    }
}
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var termArray = [Glossaries]()
    var searchedTerm  = [String]()
    var searchingTerm = false
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let algorithm = Glossaries(termName: "Algorithm", termDefinition: "A programming algorithm is a computer procedure that is a lot like a recipe (called a procedure) and tells your computer precisely what steps to take to solve a problem or reach a goal", termLink: "https://www.khanacademy.org/computing/computer-science/algorithms/intro-to-algorithms/v/what-are-algorithms")
        termArray.append(algorithm)
        
        let arrays = Glossaries(termName: "Array", termDefinition: "Arrays are containers that hold variables; they're used to group together similar variables. You can think of arrays like shelves at a pet store. The array would be the shelf, and the animals in cages are the variables inside.", termLink: "https://www.youtube.com/watch?v=NptnmWvkbTw")
        termArray.append(arrays)
        
        let bug = Glossaries(termName: "Bug", termDefinition: "an error, flaw or fault in a computer program or system that causes it to produce an incorrect or unexpected result, or to behave in unintended ways.", termLink: "https://www.youtube.com/watch?v=kRL6hjWOKWI")
        termArray.append(bug)
        
        let syntax = Glossaries(termName: "Syntax", termDefinition: "Set of rules that defines the combinations of symbols that are considered to be a correctly structured document or fragment in that language", termLink: "https://dept-info.labri.fr/~strandh/Teaching/MTP/Common/Strandh-Tutorial/syntax.html")
        termArray.append(syntax)
        
        tableView.dataSource = self
        tableView.delegate = self
        searchBar.delegate = self
    }
    
    //TableView Function
    
    /*
           DataSource Methods
           =============================
    */
    //Tells the data source to return the number of rows in a given section of a table view.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchingTerm {
            return searchedTerm.count
        } else {
            return termArray.count
        }
    }
    
    //Asks the data source for a cell to insert in a particular location of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*
         Returns a reusable table-view cell object located by its identifier.
         dequeueReusableCell With Identifier is to check first whether there is any reusable cell is available and uses that cell for creating any new cell. If there is no cell available for reuse, We can go for a new cell initialization.
         */
        let cell =  tableView.dequeueReusableCell(withIdentifier: "termstable")
        if searchingTerm {
            cell?.textLabel?.text = searchedTerm[indexPath.row]
        } else {
            cell?.textLabel?.text = termArray[indexPath.row].terms
        }
        return cell!
    }
    
    
    
    /*
            Delegate Methods
            ===========================
     */
    
    // Tells the delegate that the specified row is now selected.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showdefinition", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DefinitionScreen {
            destination.term = termArray[(tableView.indexPathForSelectedRow?.row)!]
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchedTerm  = termArray.filter { $0.terms!.contains(_, other: searchBar.text) }
        tableView.reloadData()
    }
    
    


}

